import { Component, OnInit } from '@angular/core';
import {ExpenseService} from '../../services/expense.service';
import {Expense} from '../../models/expense';

@Component({
  selector: 'app-list-expenses',
  templateUrl: './list-expenses.component.html',
  styleUrls: ['./list-expenses.component.css']
})
export class ListExpensesComponent implements OnInit {

  expenses: Expense[] = [];
  filters = {
    keyword: ''
  };

  constructor(private expenseService: ExpenseService) {
  }

  // data is an array of expenses returned from our endpoint. Now we send that directly to our filter:
  ngOnInit(): void {
    this.expenseService.getExpenses().subscribe(
      data => {
        this.expenses = data;
      }
    );
  }

  listExpenses(): void {
    this.expenseService.getExpenses().subscribe(
      expenses => {
        this.expenses = this.filterExpenses(expenses);
      });
  }

    // return Expense[] where iteration.expense(lowercase)includes filter keyword(lowercase)
  private filterExpenses(expenses: Expense[]): Expense[] {
    return expenses.filter( e => {
     return e.expense.toLowerCase().includes(this.filters.keyword.toLowerCase());
    });
  }
}
