import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AddExpenseComponent} from '../components/add-expense/add-expense.component';
import {ListExpensesComponent} from '../components/list-expenses/list-expenses.component';

const routes: Routes = [
  {path: 'expenses', component: ListExpensesComponent},
  {path: 'addexpense', component: AddExpenseComponent},
  {path: 'addexpense/:id', component: AddExpenseComponent},
  {path: '', redirectTo: '/expenses', pathMatch: 'full'},
];

@NgModule({
  exports: [RouterModule],
  imports: [
    RouterModule.forRoot(routes)
  ],
  declarations: []
})

export class AppRoutingModule { }
